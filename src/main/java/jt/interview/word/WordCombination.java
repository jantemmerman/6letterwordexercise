package jt.interview.word;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class WordCombination {
  private final List<String> components;

  WordCombination(List<String> components) {
    this.components = components;
  }

  public WordCombination() {
    this(new ArrayList<>());
  }

  WordCombination(String... components) {
    this(List.of(components));
  }

  public String getResult() {
    return String.join("", components);
  }

  public List<String> getComponents() {
    return Collections.unmodifiableList(components);
  }

  public WordCombination append(String wordPrefix) {
    List<String> newComponents = new ArrayList<>(components);
    newComponents.add(wordPrefix);
    return new WordCombination(newComponents);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    WordCombination that = (WordCombination) o;
    return Objects.equals(components, that.components);
  }

  @Override
  public int hashCode() {
    return Objects.hash(components);
  }

  @Override
  public String toString() {
    return "WordCombination{" +
      "components=" + components +
      '}';
  }
}
