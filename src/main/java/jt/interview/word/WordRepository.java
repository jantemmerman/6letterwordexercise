package jt.interview.word;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class WordRepository {
  private final Set<String> words;

  WordRepository(Collection<String> words) {
    this.words = new HashSet<>(words);
  }

  public Set<String> getWords() {
    return Collections.unmodifiableSet(words);
  }

  public Set<String> getWordsOfLength(int length) {
    return words.stream()
      .filter(word -> word.length() == length)
      .collect(Collectors.toUnmodifiableSet());
  }

  public Set<String> getWordPrefixes(String target) {
    return words.stream()
      .filter(target::startsWith)
      .collect(Collectors.toUnmodifiableSet());
  }

  public static WordRepository fromInputStream(InputStream inputStream) {
    List<String> words = new ArrayList<>();
    Scanner scanner = new Scanner(inputStream);
    while (scanner.hasNext()) {
      words.add(scanner.next());
    }
    return new WordRepository(words);
  }
}
