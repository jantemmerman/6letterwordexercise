package jt.interview.word;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordFinder {
  private final WordRepository wordRepository;

  public WordFinder(WordRepository wordRepository) {
    this.wordRepository = wordRepository;
  }

  public List<WordCombination> findCombinations(int combinationLength, int targetLength) {
    if (targetLength <= 0) {
      throw new IllegalArgumentException("Target Length should be greater than 0");
    }

    if (combinationLength <= 0) {
      throw new IllegalArgumentException("Combination length should be greater than 0");
    }

    Set<String> targets = new HashSet<>(wordRepository.getWordsOfLength(targetLength));
    List<WordCombination> allCombinations = new ArrayList<>();
    for (String target : targets) {
      List<WordCombination> targetCombinations = findCombinations(List.of(new WordCombination()), combinationLength, target);
      allCombinations.addAll(targetCombinations);
    }

    return allCombinations;
  }

  private List<WordCombination> findCombinations(List<WordCombination> wordCombinations, int maxCombinationLength, String target) {
    List<WordCombination> allWordCombinations = new ArrayList<>();

    for (WordCombination wordCombination : wordCombinations) {
      if (target.isEmpty() && maxCombinationLength == 0) {
        allWordCombinations.add(wordCombination);
      } else {
        for (String wordPrefix : wordRepository.getWordPrefixes(target)) {
          List<WordCombination> extendedWordCombination = List.of(wordCombination.append(wordPrefix));
          int remainingCombinationLength = maxCombinationLength - 1;
          String remainingTarget = target.substring(wordPrefix.length());
          allWordCombinations.addAll(findCombinations(extendedWordCombination, remainingCombinationLength, remainingTarget));
        }
      }
    }

    return allWordCombinations;
  }
}
