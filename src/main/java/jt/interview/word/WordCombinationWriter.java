package jt.interview.word;

import java.io.PrintStream;
import java.util.List;

public class WordCombinationWriter {
  private final PrintStream printStream;

  public WordCombinationWriter(PrintStream printStream) {
    this.printStream = printStream;
  }

  public void write(WordCombination wordCombination) {
    List<String> components = wordCombination.getComponents();
    String result = wordCombination.getResult();
    String line = String.format("%s=%s", String.join("+", components), result);
    printStream.println(line);
  }
}
