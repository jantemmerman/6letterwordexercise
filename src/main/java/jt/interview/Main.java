package jt.interview;

import jt.interview.word.WordCombination;
import jt.interview.word.WordCombinationWriter;
import jt.interview.word.WordFinder;
import jt.interview.word.WordRepository;

import java.io.InputStream;
import java.util.List;

public class Main {

  public void run() {
    InputStream inputStream = Main.class.getResourceAsStream("/input.txt");
    WordRepository wordRepository = WordRepository.fromInputStream(inputStream);

    WordFinder wordFinder = new WordFinder(wordRepository);
    WordCombinationWriter writer = new WordCombinationWriter(System.out);

    List<WordCombination> combinations = wordFinder.findCombinations(2, 6);
    for (WordCombination wordCombination : combinations) {
      writer.write(wordCombination);
    }
  }

  public static void main(String[] args) {
    Main main = new Main();
    main.run();
  }
}
