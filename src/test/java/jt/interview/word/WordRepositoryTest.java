package jt.interview.word;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.InputStream;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class WordRepositoryTest {

  private static final String TEST_INPUT_FILENAME = "/test_input.txt";
  private WordRepository wordRepository;

  @BeforeEach
  public void setup() {
    InputStream inputStream = WordRepositoryTest.class.getResourceAsStream(TEST_INPUT_FILENAME);
    wordRepository = WordRepository.fromInputStream(inputStream);
  }

  @Test
  public void testRepositoryReadsAllWordsFromInputStream() {
    assertThat(wordRepository.getWords()).hasSize(3);
  }

  @ParameterizedTest
  @CsvSource(value = {
    "6, foobar",
    "2, fo"
  })
  public void testFindWordsByLength(int wordLength, String expected) {
    Set<String> wordsOfLength6 = wordRepository.getWordsOfLength(wordLength);
    assertThat(wordsOfLength6).containsExactly(expected);
  }

  @Test
  public void testFindWordsByLengthShouldReturnUniqueResults() {
    WordRepository wordRepository = new WordRepository(List.of("fo", "fo", "fo"));

    Set<String> wordPrefixes = wordRepository.getWordsOfLength(2);
    assertThat(wordPrefixes).containsExactly("fo");
  }

  @Test
  public void testFindWordPrefixes() {
    Set<String> wordPrefixes = wordRepository.getWordPrefixes("foobar");
    assertThat(wordPrefixes).containsExactlyInAnyOrder("fo", "foobar");
  }

  @Test
  public void testFindWordPrefixesShouldReturnUniqueResults() {
    WordRepository wordRepository = new WordRepository(List.of("fo", "fo", "fo"));

    Set<String> wordPrefixes = wordRepository.getWordPrefixes("foobar");
    assertThat(wordPrefixes).containsExactlyInAnyOrder("fo");
  }
}
