package jt.interview.word;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class WordCombinationTest {
  WordCombination wordCombination = new WordCombination("foo", "bar");

  @Test
  public void testGetResultShouldJoinComponents() {
    assertThat(wordCombination.getResult()).isEqualTo("foobar");
  }

  @Test
  public void testAppendShouldReturnExtendedWordCombination() {
    assertThat(wordCombination.append("extension")).isEqualTo(
      new WordCombination("foo", "bar", "extension")
    );
  }

  @Test
  public void testEquals() {
    assertThat(wordCombination).isEqualTo(new WordCombination("foo", "bar"));
  }

  @Test
  public void testToString() {
    assertThat(wordCombination.toString()).isEqualTo("WordCombination{components=[foo, bar]}");
  }

}