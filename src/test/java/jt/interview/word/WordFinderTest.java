package jt.interview.word;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WordFinderTest {
  WordRepository testWordRepository = new WordRepository(List.of(
    "foobar", "fo", "obar", "o", "bar", "foo"
  ));

  WordFinder wordFinder = new WordFinder(testWordRepository);

  @Test
  public void testFindCombinationsWith2Components() {
    List<WordCombination> wordCombinations = wordFinder.findCombinations(2, 6);
    assertThat(wordCombinations).containsExactlyInAnyOrder(
      new WordCombination("foo", "bar"),
      new WordCombination("fo", "obar")
    );
  }

  @Test
  public void testFindCombinationsWith3Components() {
    List<WordCombination> wordCombinations = wordFinder.findCombinations(3, 6);
    assertThat(wordCombinations).containsExactlyInAnyOrder(
      new WordCombination("fo", "o", "bar")
    );
  }

  @Test
  public void testFindCombinationOnlyAcceptsPositiveCombinationLength() {
    assertThrows(IllegalArgumentException.class, () -> wordFinder.findCombinations(-1, 10));
  }

  @Test
  public void testFindCombinationOnlyAcceptsPositiveTargetLength() {
    assertThrows(IllegalArgumentException.class, () -> wordFinder.findCombinations(10, -1));
  }
}
